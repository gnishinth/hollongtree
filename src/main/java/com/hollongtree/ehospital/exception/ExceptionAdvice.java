package com.hollongtree.ehospital.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.hollongtree.ehospital.exceptionHandler.HospitalError;

@ControllerAdvice
public class ExceptionAdvice {
	
	@ExceptionHandler(EhospitalException.class)
	public ResponseEntity<HospitalError> ExceptionMapping(EhospitalException ec)
	{
	  HospitalError error= new HospitalError(HttpStatus.INTERNAL_SERVER_ERROR.value(),ec.getMessage());
	return new ResponseEntity<HospitalError>(error,HttpStatus.NOT_FOUND);
	  
	}
	
	public ResponseEntity<HospitalError> noContentException(EhospitalException ec)
	{
		HospitalError error= new HospitalError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ec.getMessage());
		return new ResponseEntity<HospitalError>(error,HttpStatus.NO_CONTENT);
	}
	
	public ResponseEntity<HospitalError> validException(EhospitalException ec)
	{
		HospitalError error= new HospitalError(HttpStatus.INTERNAL_SERVER_ERROR.value(), ec.getMessage());
		return new ResponseEntity<HospitalError>(error,HttpStatus.HTTP_VERSION_NOT_SUPPORTED);
	}

}
