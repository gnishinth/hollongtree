package com.hollongtree.ehospital.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.DoctorDetails;

@Repository
public interface DoctorDetailsDao extends JpaRepository<DoctorDetails, Integer> {

	DoctorDetails findByDoctorname(String doctorname);

}
