package com.hollongtree.ehospital.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.HospitalCategory;

@Repository
public interface HospitalCategoryDao extends JpaRepository<HospitalCategory, Integer>
{

	HospitalCategory findByhospitalCategoryName(String hospitalCategoryName);

}
