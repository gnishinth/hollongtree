package com.hollongtree.ehospital.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.Hospital;

@Repository
public interface HospitalDao extends JpaRepository<Hospital, Integer>
{

	Optional<Hospital> findById(Integer id);

	Hospital findByhospitalName(String hospitalName);

}
