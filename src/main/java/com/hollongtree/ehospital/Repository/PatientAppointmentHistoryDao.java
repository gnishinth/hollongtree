package com.hollongtree.ehospital.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.PatientAppointmentHistory;

@Repository
public interface PatientAppointmentHistoryDao extends JpaRepository<PatientAppointmentHistory, Integer>
{

}
