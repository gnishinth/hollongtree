package com.hollongtree.ehospital.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.MedicineList;

@Repository
public interface MedicineListDao extends JpaRepository<MedicineList, Integer>{

	MedicineList findByMedicineName(String medicineName);

}
