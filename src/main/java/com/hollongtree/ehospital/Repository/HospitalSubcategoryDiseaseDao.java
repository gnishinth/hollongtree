package com.hollongtree.ehospital.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hollongtree.ehospital.entity.HospitalSubcategoryDisease;

@Repository
public interface HospitalSubcategoryDiseaseDao extends JpaRepository<HospitalSubcategoryDisease, Integer>{

	HospitalSubcategoryDisease findByhospitalSubcategoryName(String hospitalSubcategoryName);

}
