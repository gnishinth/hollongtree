package com.hollongtree.ehospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollongtree.ehospital.Repository.HospitalCatalogueDao;
import com.hollongtree.ehospital.entity.HospitalCatalogue;
import com.hollongtree.ehospital.exception.EhospitalException;

@Service
public class HospitalCatalogueService {

	@Autowired
	HospitalCatalogueDao hospitalCatalogueDao;

	/**
	 * Save the data to the reporsitory
	 * 
	 * @param hospitalcatalogue
	 * @return
	 */
	public HospitalCatalogue addHospitalCatalogue(HospitalCatalogue hospitalcatalogue) {

		HospitalCatalogue hc = hospitalCatalogueDao.save(hospitalcatalogue);
		return hc;
	}

	/**
	 * Update the HospitalCatalogue Entity
	 * 
	 * @param hospitalCatalogue
	 * @return
	 */

	public HospitalCatalogue updateHospitalCatalogue(HospitalCatalogue hospitalCatalogue) {

		HospitalCatalogue hcat = hospitalCatalogueDao.save(hospitalCatalogue);

		return hcat;

	}

	/**
	 * fetching value from HospitalCatalogue using catalogueName if not then throw a
	 * exception
	 * 
	 * @param catalogueName
	 * @return
	 * @throws EhospitalException
	 */

	public HospitalCatalogue getHospitalCatalogue(String catalogueName) throws EhospitalException {
		HospitalCatalogue hc = hospitalCatalogueDao.findByCatalogueName(catalogueName);
		if (hc == null) {
			throw new EhospitalException("Hospital catalogue not found");
		}
		return hc;
	}

}
