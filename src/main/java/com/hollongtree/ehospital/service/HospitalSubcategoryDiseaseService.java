package com.hollongtree.ehospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.hollongtree.ehospital.Repository.HospitalSubcategoryDiseaseDao;
import com.hollongtree.ehospital.entity.HospitalSubcategoryDisease;
import com.hollongtree.ehospital.exception.EhospitalException;

@Service
public class HospitalSubcategoryDiseaseService {
	@Autowired
	HospitalSubcategoryDiseaseDao hospitalSubcategoryDiseaseDao;
 
	/**save the data to the reporsitory
	 * 
	 * @param hospitalSubcategoryDisease
	 * @return returning the entity
	 */
	public HospitalSubcategoryDisease addHospitalSubcategoryDisease(
			HospitalSubcategoryDisease hospitalSubcategoryDisease) {

		HospitalSubcategoryDisease hsd = hospitalSubcategoryDiseaseDao.save(hospitalSubcategoryDisease);
		
		return hsd;
	}
	/**logic for update the HospitalSubcategoryDisease entity
	 * 
	 * @param hospitalSubcategoryDisease
	 * @return
	 */

	public HospitalSubcategoryDisease updateHospitalSubcategoryDisease(
			@RequestBody HospitalSubcategoryDisease hospitalSubcategoryDisease) {

		HospitalSubcategoryDisease hsd = hospitalSubcategoryDiseaseDao.save(hospitalSubcategoryDisease);
		return hsd;
	}

	/**fetching the data using HospitalSubcategoryname
	 * if not present then throwing a exception message
	 * @param hospitalSubcategoryName
	 * @return
	 * @throws EhospitalException
	 */
	public HospitalSubcategoryDisease getHospitalSubcategoryDisease(String hospitalSubcategoryName) throws EhospitalException {
		HospitalSubcategoryDisease hsd = hospitalSubcategoryDiseaseDao
				.findByhospitalSubcategoryName(hospitalSubcategoryName);
		if (hsd == null) {
			throw new EhospitalException("hospitalSubcategory Disease Name not found");
		}
		return hsd;
	}

}
