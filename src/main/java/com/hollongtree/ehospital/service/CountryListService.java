package com.hollongtree.ehospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollongtree.ehospital.Repository.CountryListDao;
import com.hollongtree.ehospital.entity.CountryList;
import com.hollongtree.ehospital.exception.EhospitalException;

@Service
public class CountryListService {
	
	@Autowired
	CountryListDao countryListDao;

	/**
	 * Method for saving the data in the database
	 * @param countryList
	 * @return
	 */
	public CountryList addCountryList(CountryList countryList) {
		
		CountryList cl=countryListDao.save(countryList);
		return cl;
		
	}
	/**
	 * Method for updating the record
	 * @param countryList
	 * @return
	 */

	public CountryList updateCountryList(CountryList countryList) {
		
		CountryList cl=countryListDao.save(countryList);
		return cl;
		
	}

	/**
	 * Method for fetching the data, if not found then throw a exception
	 * @param countryName
	 * @return
	 * @throws EhospitalException
	 */
	public CountryList addCountryList(String countryName) throws EhospitalException {
		CountryList cl=countryListDao.findByCountryName(countryName);
		if(cl==null)
		{
			throw new EhospitalException("Country not found");
		}
		return cl;
	}

}
