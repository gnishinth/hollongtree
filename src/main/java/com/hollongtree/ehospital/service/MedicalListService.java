package com.hollongtree.ehospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollongtree.ehospital.Repository.MedicalListDao;
import com.hollongtree.ehospital.entity.MedicalList;
import com.hollongtree.ehospital.exception.EhospitalException;

@Service
public class MedicalListService {
	
	@Autowired
	MedicalListDao medicalListDao;

	/**
	 * method for Save the medicalList data
	 * @param medicalList
	 * @return
	 */
	public MedicalList addMedicalList(MedicalList medicalList) {
		MedicalList ml=medicalListDao.save(medicalList);
		return ml;
	}

	/**
	 * method for Update the medicalList entity
	 * @param medicalList
	 * @return
	 */
	public MedicalList updateMedicalList(MedicalList medicalList) {
		
		MedicalList ml=medicalListDao.save(medicalList);
		return ml;
	}

	/**
	 * method for fetching the medicallist using medicalname if not found then throw a exception
	 * @param medicalName
	 * @return
	 * @throws EhospitalException
	 */
	public MedicalList getMedicalList(String medicalName) throws EhospitalException {
		MedicalList ml=medicalListDao.findByMedicalName(medicalName);
		if(ml==null)
		{
			throw new EhospitalException("medical name not found");
		}
		return ml;
	}
	

}
