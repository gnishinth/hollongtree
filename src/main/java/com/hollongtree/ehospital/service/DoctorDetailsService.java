package com.hollongtree.ehospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollongtree.ehospital.Repository.DoctorDetailsDao;
import com.hollongtree.ehospital.entity.DoctorDetails;
import com.hollongtree.ehospital.exception.EhospitalException;

@Service
public class DoctorDetailsService {

	@Autowired
	DoctorDetailsDao doctorDetailsDao;

	/**
	 * Method for saving the data to the databasedoctor_details
	 * 
	 * @param doctordetails
	 * @return
	 */
	public DoctorDetails addDoctorDetails(DoctorDetails doctordetails) {

		DoctorDetails dd = doctorDetailsDao.save(doctordetails);
		return dd;

	}

	/**
	 * Method for Updating the record
	 * 
	 * @param doctordetails
	 * @return
	 */

	public DoctorDetails updateDoctorDetails(DoctorDetails doctordetails) {

		DoctorDetails dd = doctorDetailsDao.save(doctordetails);
		return dd;

	}

	/**
	 * Method for fetching the data from database if not found then throw a
	 * exception
	 * 
	 * @param doctorname
	 * @return
	 * @throws EhospitalException
	 */

	public DoctorDetails getDoctorDetails(String doctorname) throws EhospitalException {
		DoctorDetails dd = doctorDetailsDao.findByDoctorname(doctorname);
		if (dd == null) {
			throw new EhospitalException("doctor name not found");
		}
		return dd;
	}

}
