package com.hollongtree.ehospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollongtree.ehospital.Repository.CityListDao;
import com.hollongtree.ehospital.entity.CityList;
import com.hollongtree.ehospital.exception.EhospitalException;

@Service
public class CityListService {
	
	@Autowired
	CityListDao cityListDao;

	/**
	 * Save the data to the Database
	 * @param cityList
	 * @return
	 */
	public CityList addCityList(CityList cityList) {
		
		CityList cl=cityListDao.save(cityList);
		return cl;
		
	}
	/**
	 * this function is for save the updated value 
	 * @param cityList
	 * @return
	 */

	public CityList updateCityList(CityList cityList) {
		
		CityList cl=cityListDao.save(cityList);
		return cl;
		
	}
	/**
	 * This function is for fetchinf value if not found then throwing a exception
	 * @param cityName
	 * @return
	 * @throws EhospitalException
	 */

	public CityList getCityList(String cityName) throws EhospitalException {
		CityList cl=cityListDao.findBycityName(cityName);
		if(cl==null)
		{
			throw new EhospitalException("City name not found");
		}
		return cl;
	}

}
