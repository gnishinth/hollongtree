package com.hollongtree.ehospital.service;

import com.hollongtree.ehospital.Repository.LoginRepository;
import com.hollongtree.ehospital.Repository.PatientRepository;
import com.hollongtree.ehospital.entity.LoginHistory;
import com.hollongtree.ehospital.entity.Patient;
import com.hollongtree.ehospital.exception.HospitalException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LoginService {

    @Autowired
    LoginRepository loginRepository;

    @Autowired
    PatientRepository patientRepository;

    public LoginHistory getLoginHistoryForPatient(String patientId)  {

       Optional <LoginHistory> loginHistory= loginRepository.findById(Integer.valueOf(patientId));

         return loginRepository.findById(Integer.valueOf(patientId)).get();

       }

    public List<LoginHistory> getLoginHistoryForALLPatient()  {

       return loginRepository.findAll();

    }


    }





