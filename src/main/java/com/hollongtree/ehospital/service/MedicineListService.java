package com.hollongtree.ehospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.hollongtree.ehospital.Repository.MedicineListDao;
import com.hollongtree.ehospital.entity.MedicineList;
import com.hollongtree.ehospital.exception.EhospitalException;

@Service
public class MedicineListService {
	
	@Autowired
	MedicineListDao medicineListDao;

	/**
	 * Method for saving the data
	 * @param medicineList
	 * @return
	 */
	public MedicineList addMedicineList(MedicineList medicineList) {
		
		MedicineList ml=medicineListDao.save(medicineList);
		return ml;
		
	}

	/**
	 * Method for updating medicinelist entity
	 * @param medicineList
	 * @return
	 */
	public MedicineList updateMedicineList(@RequestBody MedicineList medicineList) {
		
		MedicineList ml=medicineListDao.save(medicineList);
		return ml;
		
	}

	/**
	 * Method for fetching the data using medicine name, if not found then throw a Exception
	 * @param medicineName
	 * @return
	 * @throws EhospitalException
	 */
	public MedicineList getMedicineList(String medicineName) throws EhospitalException {
		MedicineList ml=medicineListDao.findByMedicineName(medicineName);
		if(ml==null)
		{
			throw new EhospitalException("medicine name not found");
		}
		return ml;
	}

}
