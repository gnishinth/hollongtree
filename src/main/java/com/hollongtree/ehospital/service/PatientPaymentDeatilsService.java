package com.hollongtree.ehospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollongtree.ehospital.Repository.PatientPaymentDetailsDao;
import com.hollongtree.ehospital.entity.PatientPaymentDetails;
import com.hollongtree.ehospital.exception.EhospitalException;

@Service
public class PatientPaymentDeatilsService {
	
	@Autowired
	PatientPaymentDetailsDao patientPaymentDetailsDao;

	/**
	 * Method for save the data to the db
	 * @param patientPaymentDetails
	 * @return
	 */
	public PatientPaymentDetails addPatientPaymentDetails(PatientPaymentDetails patientPaymentDetails) {
		
		PatientPaymentDetails ppd=patientPaymentDetailsDao.save(patientPaymentDetails);
		return ppd;
	}

	/**
	 * Method for Update the entity
	 * @param patientPaymentDeatils
	 * @return
	 */
	public PatientPaymentDetails updatePatientPaymentDetails(PatientPaymentDetails patientPaymentDeatils) {
		
		PatientPaymentDetails ppd=patientPaymentDetailsDao.save(patientPaymentDeatils);
		return ppd;
	}

	/**
	 * Method for fetching the data using patientsname, if not found then throw a exception
	 * @param patientsName
	 * @return
	 * @throws EhospitalException
	 */
	public PatientPaymentDetails getPatientPaymentDetails(String patientsName) throws EhospitalException {
		PatientPaymentDetails ppd=patientPaymentDetailsDao.findByPatientsName(patientsName);
		if(ppd==null)
		{
			throw new EhospitalException("patient name not found");
		}
		return ppd;
	}

}
