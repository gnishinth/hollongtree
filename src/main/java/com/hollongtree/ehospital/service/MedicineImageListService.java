package com.hollongtree.ehospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.hollongtree.ehospital.Repository.MedicineImageListDao;
import com.hollongtree.ehospital.entity.MedicineImageList;

@Service
public class MedicineImageListService {
	
	@Autowired
	MedicineImageListDao medicineImageListDao;

	/**
	 * Method for save the data
	 * @param medicineImageList
	 * @return
	 */
	public MedicineImageList addMedicineImageList(MedicineImageList medicineImageList) {
		
		MedicineImageList mil=medicineImageListDao.save(medicineImageList);
		return mil;
	}

	/**
	 * Method for update the medicineImage
	 * @param medicineImageList
	 * @return
	 */
	public MedicineImageList updateMedicineImageList(@RequestBody MedicineImageList medicineImageList) {
		MedicineImageList mil=medicineImageListDao.save(medicineImageList);
		return mil;
	}

}
