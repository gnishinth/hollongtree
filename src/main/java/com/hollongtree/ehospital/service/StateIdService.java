package com.hollongtree.ehospital.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollongtree.ehospital.Repository.StateIdDao;
import com.hollongtree.ehospital.entity.StateId;
import com.hollongtree.ehospital.exception.EhospitalException;

@Service
public class StateIdService {
	
	@Autowired
	StateIdDao stateIdDao;

	/**
	 * Method for adding the data to the database
	 * @param stateId
	 * @return
	 */
	public StateId addStateId(StateId stateId)   {
		StateId sd=stateIdDao.save(stateId);
		
		
		return sd;
		
	}
	/**
	 * Method for Updating the records in the database
	 * @param stateId
	 * @return
	 */

	public StateId updateStateId(StateId stateId) {
		
		StateId sd=stateIdDao.save(stateId);
		return sd;
		
	}

	/**Method for fetching data from stateId entity using statename, if not found then throw a exception
	 * 
	 * @param stateName
	 * @return return type is list for fetch list of data
	 * @throws EhospitalException
	 */
	public List<StateId> getstateId(String stateName) throws EhospitalException  {
		List<StateId> sd=stateIdDao.findByStateName(stateName);
		if(sd.isEmpty())
		{
			throw new EhospitalException("not found");
		}
		
		
		return sd;
	}
	

}
