package com.hollongtree.ehospital.service;

import static org.assertj.core.api.Assertions.catchThrowable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.Repository.HospitalCategoryDao;
import com.hollongtree.ehospital.entity.HospitalCategory;

@Service
public class HospitalCategoryService {

	@Autowired
	HospitalCategoryDao hospitalCategoryDao;

	/**
	 * save the data to the reporsitory
	 * 
	 * @param hospitalCategory
	 * @return
	 */
	public HospitalCategory addHospitalCategory(HospitalCategory hospitalCategory) {

		HospitalCategory hc = hospitalCategoryDao.save(hospitalCategory);

		return hc;

	}

	/**
	 * Update the HospitalCategory entity
	 * 
	 * @param hospitalCategory
	 * @return
	 */
	public HospitalCategory updateHospitalCategory(@RequestBody HospitalCategory hospitalCategory) {

		HospitalCategory hc = hospitalCategoryDao.save(hospitalCategory);
		return hc;

	}

	/**
	 * fetching the values from HospitalCategory using HospitalCategoryName if not
	 * found then throw a exception
	 * 
	 * @param hospitalCategoryName
	 * @return
	 * @throws EhospitalException
	 */

	public HospitalCategory getCategory(String hospitalCategoryName) throws EhospitalException {

		HospitalCategory hc = hospitalCategoryDao.findByhospitalCategoryName(hospitalCategoryName);
		if (hc == null) {
			throw new EhospitalException("hospitalCategoryName not found");
		}
		return hc;

	}

}
