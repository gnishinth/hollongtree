package com.hollongtree.ehospital.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollongtree.ehospital.Repository.PatientAppointmentHistoryDao;
import com.hollongtree.ehospital.entity.PatientAppointmentHistory;

@Service
public class PatientAppointmentHistoryService {
	
	@Autowired
	PatientAppointmentHistoryDao patientAppointmentHistoryDao;

	/**
	 * method for save the patientAppointmentHistory
	 * @param patientAppointmentHistory
	 * @return
	 */
	public PatientAppointmentHistory addPatientAppointmentHistory(PatientAppointmentHistory patientAppointmentHistory) {
		
		PatientAppointmentHistory pah=patientAppointmentHistoryDao.save(patientAppointmentHistory);
		return pah;
	}

	/**
	 * Method for Update the patientAppointmentHistory
	 * @param patientAppointmentHistory
	 * @return
	 */
	public PatientAppointmentHistory updatePatientAppointmentHistory(PatientAppointmentHistory patientAppointmentHistory) {

		PatientAppointmentHistory pah=patientAppointmentHistoryDao.save(patientAppointmentHistory);
        return pah;
	}
	
	
	

}
