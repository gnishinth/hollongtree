package com.hollongtree.ehospital.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "hospital_subcategory_disease")
public class HospitalSubcategoryDisease {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "hospital_subcategory_id")
	private int hospitalSubcategoryId;
	@Column(name = "hospital_subcategory_name", length = 150)
	private String hospitalSubcategoryName;
	@Column(name = "hospital_subcategory_image_path", length = 150)
	private String hospitalSubcategoryImagePath;
	@ManyToOne
	private HospitalCategory hospital_category;

	public HospitalSubcategoryDisease() {
	}

	public HospitalSubcategoryDisease(int hospitalsubcategoryid, String hospitalsubcategoryname,
			String hospitalsubcategoryimage_path, HospitalCategory hospital_category) {
		super();
		this.hospitalSubcategoryId = hospitalsubcategoryid;
		this.hospitalSubcategoryName = hospitalsubcategoryname;
		this.hospitalSubcategoryImagePath = hospitalsubcategoryimage_path;
		// this.hospital_category = hospital_category;
	}

	public int getHospitalsubcategoryid() {
		return hospitalSubcategoryId;
	}

	public void setHospitalsubcategoryid(int hospitalsubcategoryid) {
		this.hospitalSubcategoryId = hospitalsubcategoryid;
	}

	public String getHospitalsubcategoryname() {
		return hospitalSubcategoryName;
	}

	public void setHospitalsubcategoryname(String hospitalsubcategoryname) {
		this.hospitalSubcategoryName = hospitalsubcategoryname;
	}

	public String getHospitalsubcategoryimage_path() {
		return hospitalSubcategoryImagePath;
	}

	public void setHospitalsubcategoryimage_path(String hospitalsubcategoryimage_path) {
		this.hospitalSubcategoryImagePath = hospitalsubcategoryimage_path;
	}
	
	  public HospitalCategory getHospital_category() 
	  {
		  return hospital_category;
		  }
	  
	  
	  public void setHospital_category(HospitalCategory hospital_category) {
	  this.hospital_category = hospital_category; }
	  
	 

}