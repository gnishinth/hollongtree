package com.hollongtree.ehospital.entity;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "doctor_login")
public class DoctorLogin {
	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "doctor_login_id")
	private int doctorloginid;
	@Column(name = "doctor_email",length = 100)
	private String doctoremail;
	@Column(name = "doctor_passsword",length = 25)
	private String doctorpasssword;
	@OneToOne
	private DoctorDetails doctor_details;
	
	public DoctorLogin() {
		
	}

	public DoctorLogin(int doctorloginid, String doctoremail, String doctorpasssword, DoctorDetails doctor_details) {
		super();
		this.doctorloginid = doctorloginid;
		this.doctoremail = doctoremail;
		this.doctorpasssword = doctorpasssword;
		this.doctor_details = doctor_details;
	}

	public int getDoctorloginid() {
		return doctorloginid;
	}

	public void setDoctorloginid(int doctorloginid) {
		this.doctorloginid = doctorloginid;
	}

	public String getDoctoremail() {
		return doctoremail;
	}

	public void setDoctoremail(String doctoremail) {
		this.doctoremail = doctoremail;
	}

	public String getDoctorpasssword() {
		return doctorpasssword;
	}

	public void setDoctorpasssword(String doctorpasssword) {
		this.doctorpasssword = doctorpasssword;
	}

	public DoctorDetails getDoctor_details() {
		return doctor_details;
	}

	public void setDoctor_details(DoctorDetails doctor_details) {
		this.doctor_details = doctor_details;
	}
	
	

}