package com.hollongtree.ehospital.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "patient_payment_details")
public class PatientPaymentDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "patients_appointment_details_id")
	private int patientsAppointmentDetailsId;
	@Column(name = "patients_name")
	private String patientsName;
	@Column(name = "patients_id")
	private int patientsId;
	@Column(name = "validity_from_time")
	private Date validityFromTime;
	@Column(name = "validity_end_time")
	private Date validityEndTime;
	@Column(name = "doctor_details_id")
	@ElementCollection(targetClass =DoctorDetails.class )
	private Set<DoctorDetails> doctorDetailsId=new HashSet<DoctorDetails>();
	@Column(name = "validity")
	private int validity;
	@Column(name = "payment_status")
	private String paymentStatus;
	@Column(name = "payment_amount")
	private double paymentAmount;
	@Column(name = "udf1")
	private String udf1;
	@Column(name = "udf2")
	private String udf2;
	@Column(name = "udf3")
	private String udf3;
	@Column(name = "udf4")
	private String udf4;
	@Column(name = "udf5")
	private String udf5;
	@Column(name = "payment_id")
	private String paymentId;
	@Column(name = "remarks")
	private String remarks;
	@Column(name = "patients_subcategory_id")
	private int patientsSubcategoryId;
	
	public PatientPaymentDetails(){
		
	}

	public PatientPaymentDetails(int patientsAppointmentDetailsId, String patientsName, int patientsId,
			Date validityFromTime, Date validityEndTime, Set<DoctorDetails> doctorDetailsId, int validity, String paymentStatus,
			double paymentAmount, String udf1, String udf2, String udf3, String udf4, String udf5, String paymentId,
			String remarks, int patientsSubcategoryId) {
		super();
		this.patientsAppointmentDetailsId = patientsAppointmentDetailsId;
		this.patientsName = patientsName;
		this.patientsId = patientsId;
		this.validityFromTime = validityFromTime;
		this.validityEndTime = validityEndTime;
		this.doctorDetailsId = doctorDetailsId;
		this.validity = validity;
		this.paymentStatus = paymentStatus;
		this.paymentAmount = paymentAmount;
		this.udf1 = udf1;
		this.udf2 = udf2;
		this.udf3 = udf3;
		this.udf4 = udf4;
		this.udf5 = udf5;
		this.paymentId = paymentId;
		this.remarks = remarks;
		this.patientsSubcategoryId = patientsSubcategoryId;
	}

	public int getPatientsAppointmentDetailsId() {
		return patientsAppointmentDetailsId;
	}

	public void setPatientsAppointmentDetailsId(int patientsAppointmentDetailsId) {
		this.patientsAppointmentDetailsId = patientsAppointmentDetailsId;
	}

	public String getPatientsName() {
		return patientsName;
	}

	public void setPatientsName(String patientsName) {
		this.patientsName = patientsName;
	}

	public int getPatientsId() {
		return patientsId;
	}

	public void setPatientsId(int patientsId) {
		this.patientsId = patientsId;
	}

	public Date getValidityFromTime() {
		return validityFromTime;
	}

	public void setValidityFromTime(Date validityFromTime) {
		this.validityFromTime = validityFromTime;
	}

	public Date getValidityEndTime() {
		return validityEndTime;
	}

	public void setValidityEndTime(Date validityEndTime) {
		this.validityEndTime = validityEndTime;
	}

    @ManyToMany(fetch=FetchType.LAZY, mappedBy= "Patient_payment_details")
	public Set<DoctorDetails> getDoctorDetailsId() {
		return doctorDetailsId;
	}

	public void setDoctorDetailsId(Set<DoctorDetails> doctorDetailsId) {
		this.doctorDetailsId = doctorDetailsId;
	}

	public int getValidity() {
		return validity;
	}

	public void setValidity(int validity) {
		this.validity = validity;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getUdf1() {
		return udf1;
	}

	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}

	public String getUdf2() {
		return udf2;
	}

	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}

	public String getUdf3() {
		return udf3;
	}

	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}

	public String getUdf4() {
		return udf4;
	}

	public void setUdf4(String udf4) {
		this.udf4 = udf4;
	}

	public String getUdf5() {
		return udf5;
	}

	public void setUdf5(String udf5) {
		this.udf5 = udf5;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public int getPatientsSubcategoryId() {
		return patientsSubcategoryId;
	}

	public void setPatientsSubcategoryId(int patientsSubcategoryId) {
		this.patientsSubcategoryId = patientsSubcategoryId;
	}
	
	
	

}
