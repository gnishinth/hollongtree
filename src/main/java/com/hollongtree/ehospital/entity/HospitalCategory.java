package com.hollongtree.ehospital.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "hospital_category")
public class HospitalCategory {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "hospital_category_id")
	private int hospitalCategoryId;
	@Column(name = "hospital_category_name", length = 100)
	private String hospitalCategoryName;
	@Column(name = "hospital_category_path", length = 150)
	private String hospitalCategoryPath;
	@OneToMany(targetEntity = HospitalSubcategoryDisease.class,cascade = CascadeType.ALL)
	@JoinColumn(name = "hospital_subcategory_fk_id",referencedColumnName = "hospital_category_id")
	private List<HospitalSubcategoryDisease> hospitalSubcategoryDisease;
	
	public HospitalCategory() {
	}

	public HospitalCategory(int hospitalcategoryid, String hospitalcategoryname, String hospitalcategorypath,
			List<HospitalSubcategoryDisease> hospital_Subcategory_Disease) {
		super();
		this.hospitalCategoryId = hospitalcategoryid;
		this.hospitalCategoryName = hospitalcategoryname;
		this.hospitalCategoryPath = hospitalcategorypath;
		this.hospitalSubcategoryDisease = hospital_Subcategory_Disease;
	}

	public int getHospitalcategoryid() {
		return hospitalCategoryId;
	}

	public void setHospitalcategoryid(int hospitalcategoryid) {
		this.hospitalCategoryId = hospitalcategoryid;
	}

	public String getHospitalcategoryname() {
		return hospitalCategoryName;
	}

	public void setHospitalcategoryname(String hospitalcategoryname) {
		this.hospitalCategoryName = hospitalcategoryname;
	}

	public String getHospitalcategorypath() {
		return hospitalCategoryPath;
	}

	public void setHospitalcategorypath(String hospitalcategorypath) {
		this.hospitalCategoryPath = hospitalcategorypath;
	}

	public List<HospitalSubcategoryDisease> getHospital_Subcategory_Disease() {
		return hospitalSubcategoryDisease;
	}

	public void setHospital_Subcategory_Disease(List<HospitalSubcategoryDisease> hospital_Subcategory_Disease) {
		this.hospitalSubcategoryDisease = hospital_Subcategory_Disease;
	}
	
	

}