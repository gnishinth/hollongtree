package com.hollongtree.ehospital.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "doctor_details")
public class DoctorDetails {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "doctor_details_id")
	private int doctordetailsid;
	@Column(name = "doctor_name",length = 100)
	private String doctorname;
	@Column(name = "doctor_address",length = 255)
	private String doctoraddress;
	@Column(name = "hospital_subcategory_id")
	private int hospitalsubcategoryid;
	@Column(name = "active")
	private boolean active;
	@Column(name = "state_id")
	private int stateid;
	@Column(name = "city_id")
	private int cityid;
	@Column(name = "pincode")
	private String pincode;
	@Column(name = "country_id")
	private int countryid;
	@Column(name = "doctor_fee")
	private double doctorfee;
	@Column(name = "hospital_category_id")
	@ElementCollection(targetClass = HospitalCategory.class)
	private Set<HospitalCategory> hospitalCategoryId= new HashSet<HospitalCategory>();
	@Column(name = "hospital_catalogue_id")
	@ElementCollection(targetClass = HospitalCatalogue.class)
	private Set<HospitalCatalogue> hospitalCatalogueId=new HashSet<HospitalCatalogue>();
	@Column(name = "udf1",length = 100)
	private String udf1;
	@Column(name = "udf2",length = 100)
	private String udf2;
	@Column(name = "udf3",length = 100)
	private String udf3;
	@Column(name = "udf4",length = 100)
	private String udf4;
	@Column(name = "udf5",length = 100)
	private String udf5;
	@Column(name = "aggrement_id",length = 100)
	private int aggrementId;
	@Column(name = "image_path",length = 100)
	private String imagePath;
	@Column(name = "charges")
	private double charges;
	@Column(name = "charges_type")
	private String chargesType;
	@Column(name = "no_of_patient")
	private int noOfPatient;
	@Column(name = "from_time")
	private String fromTime;
	@Column(name = "end_time")
	private String endTime;
	@Column(name = "active_time")
	private int activeTime;
	@Column(name = "charges_applicable")
	private boolean chargesApplicable;
	@Column(name = "one_patients_validity")
	private int onePatientsValidity;
	@Column(name = "doctor_email")
	private String doctorEmail;
	@Column(name = "doctor_password")
	private String doctorPassword;
	
	public DoctorDetails() {
		
	}

	public DoctorDetails(int doctordetailsid, String doctorname, String doctoraddress, int hospitalsubcategoryid,
			boolean active, int stateid, int cityid, String pincode, int countryid, double doctorfee,
			Set<HospitalCategory> hospitalcategoryid, Set<HospitalCatalogue> hospitalcatalogueid, String udf1, String udf2, String udf3, String udf4,
			String udf5, int aggrementid, String imagepath, double charges, String chargestype, int noofpatient,
			String fromtime, String endtime, int activetime, boolean chargesapplicable, int onepatientsvalidity,
			String doctoremail, String doctorpassword) {
		super();
		this.doctordetailsid = doctordetailsid;
		this.doctorname = doctorname;
		this.doctoraddress = doctoraddress;
		this.hospitalsubcategoryid = hospitalsubcategoryid;
		this.active = active;
		this.stateid = stateid;
		this.cityid = cityid;
		this.pincode = pincode;
		this.countryid = countryid;
		this.doctorfee = doctorfee;
		this.hospitalCategoryId = hospitalcategoryid;
		this.hospitalCatalogueId = hospitalcatalogueid;
		this.udf1 = udf1;
		this.udf2 = udf2;
		this.udf3 = udf3;
		this.udf4 = udf4;
		this.udf5 = udf5;
		this.aggrementId = aggrementid;
		this.imagePath = imagepath;
		this.charges = charges;
		this.chargesType = chargestype;
		this.noOfPatient = noofpatient;
		this.fromTime = fromtime;
		this.endTime = endtime;
		this.activeTime = activetime;
		this.chargesApplicable = chargesapplicable;
		this.onePatientsValidity = onepatientsvalidity;
		this.doctorEmail = doctoremail;
		this.doctorPassword = doctorpassword;
	}

	public int getDoctordetailsid() {
		return doctordetailsid;
	}

	public void setDoctordetailsid(int doctordetailsid) {
		this.doctordetailsid = doctordetailsid;
	}

	public String getDoctorname() {
		return doctorname;
	}

	public void setDoctorname(String doctorname) {
		this.doctorname = doctorname;
	}

	public String getDoctoraddress() {
		return doctoraddress;
	}

	public void setDoctoraddress(String doctoraddress) {
		this.doctoraddress = doctoraddress;
	}


	public int getHospitalsubcategoryid() {
		return hospitalsubcategoryid;
	}

	public void setHospitalsubcategoryid(int hospitalsubcategoryid) {
		this.hospitalsubcategoryid = hospitalsubcategoryid;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public int getStateid() {
		return stateid;
	}

	public void setStateid(int stateid) {
		this.stateid = stateid;
	}

	public int getCityid() {
		return cityid;
	}

	public void setCityid(int cityid) {
		this.cityid = cityid;
	}

	public String getPincode() {
		return pincode;
	}

	public void setPincode(String pincode) {
		this.pincode = pincode;
	}

	public int getCountryid() {
		return countryid;
	}

	public void setCountryid(int countryid) {
		this.countryid = countryid;
	}

	public double getDoctorfee() {
		return doctorfee;
	}

	public void setDoctorfee(double doctorfee) {
		this.doctorfee = doctorfee;
	}


	@OneToMany(fetch = FetchType.LAZY,mappedBy = "doctor_details")
	public Set<HospitalCategory> getHospitalcategoryid() {
		return hospitalCategoryId;
	}

    
	public void Hospitalcategoryid(Set<HospitalCategory> hospitalcategoryid) {
		this.hospitalCategoryId = hospitalcategoryid;
	}


	@OneToMany(fetch = FetchType.LAZY,mappedBy = "doctor_details")
	public Set<HospitalCatalogue> getHospitalcatalogueid() {
		return hospitalCatalogueId;
	}

	public void setHospitalcatalogueid(Set<HospitalCatalogue> hospitalcatalogueid) {
		this.hospitalCatalogueId = hospitalcatalogueid;
	}

	public String getUdf1() {
		return udf1;
	}

	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}

	public String getUdf2() {
		return udf2;
	}

	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}

	public String getUdf3() {
		return udf3;
	}

	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}

	public String getUdf4() {
		return udf4;
	}

	public void setUdf4(String udf4) {
		this.udf4 = udf4;
	}

	public String getUdf5() {
		return udf5;
	}

	public void setUdf5(String udf5) {
		this.udf5 = udf5;
	}

	public int getAggrementid() {
		return aggrementId;
	}

	public void setAggrementid(int aggrementid) {
		this.aggrementId = aggrementid;
	}

	public String getImagepath() {
		return imagePath;
	}

	public void setImagepath(String imagepath) {
		this.imagePath = imagepath;
	}

	public double getCharges() {
		return charges;
	}

	public void setCharges(double charges) {
		this.charges = charges;
	}

	public String getChargestype() {
		return chargesType;
	}

	public void setChargestype(String chargestype) {
		this.chargesType = chargestype;
	}

	public int getNoofpatient() {
		return noOfPatient;
	}

	public void setNoofpatient(int noofpatient) {
		this.noOfPatient = noofpatient;
	}

	public String getFromtime() {
		return fromTime;
	}

	public void setFromtime(String fromtime) {
		this.fromTime = fromtime;
	}

	public String getEndtime() {
		return endTime;
	}

	public void setEndtime(String endtime) {
		this.endTime = endtime;
	}

	public int getActivetime() {
		return activeTime;
	}

	public void setActivetime(int activetime) {
		this.activeTime = activetime;
	}

	public boolean isChargesapplicable() {
		return chargesApplicable;
	}

	public void setChargesapplicable(boolean chargesapplicable) {
		this.chargesApplicable = chargesapplicable;
	}

	public int getOnepatientsvalidity() {
		return onePatientsValidity;
	}

	public void setOnepatientsvalidity(int onepatientsvalidity) {
		this.onePatientsValidity = onepatientsvalidity;
	}

	public String getDoctoremail() {
		return doctorEmail;
	}

	public void setDoctoremail(String doctoremail) {
		this.doctorEmail = doctoremail;
	}

	public String getDoctorpassword() {
		return doctorPassword;
	}

	public void setDoctorpassword(String doctorpassword) {
		this.doctorPassword = doctorpassword;
	}

}