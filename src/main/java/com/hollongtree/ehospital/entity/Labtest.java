package com.hollongtree.ehospital.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
@Entity
@Table(name = "Labtest")
public class Labtest {

    private Integer labtestid;

    private int labtestname;

    private List<Hospital> hospitalid;


    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "labtest_id")
    public Integer getLabtestid() {
        return labtestid;
    }

    public void setLabtestid(Integer labtestid) {
        this.labtestid = labtestid;
    }

    @Column(name="labtest_name")
    public int getLabtestname() {
        return labtestname;
    }

    public void setLabtestname(int labtestname) {
        this.labtestname = labtestname;
    }

    @OneToMany(targetEntity = Hospital.class, cascade = CascadeType.ALL)
   	@JoinColumn(name = "hospital_fk_id",referencedColumnName = "labtest_id")
   	public List<Hospital> getHospitalid() {
   		return hospitalid;
   	}
   	
   	public void setHospitalid(List<Hospital> hospitalid) {
   		this.hospitalid = hospitalid;
   	}
}