package com.hollongtree.ehospital.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "city_list")
public class CityList {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "city_id")
	private int cityId;
	@Column(name = "city_name")
	private String cityName;
	
	public CityList() {
		
	}

	public CityList(int cityid, String cityname) {
		super();
		this.cityId = cityid;
		this.cityName = cityname;
	}

	public int getCityid() {
		return cityId;
	}

	public void setCityid(int cityid) {
		this.cityId = cityid;
	}

	public String getCityname() {
		return cityName;
	}

	public void setCityname(String cityname) {
		this.cityName = cityname;
	}
	

}
