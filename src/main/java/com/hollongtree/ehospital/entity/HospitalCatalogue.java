package com.hollongtree.ehospital.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "hospital_catalogue")
public class HospitalCatalogue {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "hospital_catalogue_id")
	private int hospitalCatalogueId;
	@Column(name = "catalogue_name",length = 100)
	private String catalogueName;
	@Column(name = "catalogue_image_path",length = 100)
	private String catalogueImagePath;
	@OneToMany(targetEntity = HospitalCategory.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "hospital_category_fk_id",referencedColumnName = "hospital_catalogue_id")
	private List<HospitalCategory> hospital_category;
	
	public HospitalCatalogue() {
	}
	
	

	public HospitalCatalogue(int hospitalcatalogueid, String cataloguename, String catalogueimagepath) {
		super();
		this.hospitalCatalogueId = hospitalcatalogueid;
		this.catalogueName = cataloguename;
		this.catalogueImagePath = catalogueimagepath;
	}



	public int getHospitalcatalogueid() {
		return hospitalCatalogueId;
	}

	public void setHospitalcatalogueid(int hospitalcatalogueid) {
		this.hospitalCatalogueId = hospitalcatalogueid;
	}

	public String getCataloguename() {
		return catalogueName;
	}

	public void setCataloguename(String cataloguename) {
		this.catalogueName = cataloguename;
	}

	public String getCatalogueimagepath() {
		return catalogueImagePath;
	}

	public void setCatalogueimagepath(String catalogueimagepath) {
		this.catalogueImagePath = catalogueimagepath;
	}
}
	
	