package com.hollongtree.ehospital.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "hospital")
public class Hospital {
	@Id
	//@GeneratedValue(strategy = GenerationType.AUTO)
	@GeneratedValue
	@Column(name = "hospital_id")
	private Integer hospitalId;
	
	@Column(name = "hospital_address", length = 255)
	private String hospitalAddress;
	
	@Column(name = "hospital_name", length = 100)
	private String hospitalName;
	
	@Column(name = "hospital_image_path", length = 100)
	private String hospitalImagePath;
	
    @OneToMany(targetEntity = HospitalCatalogue.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "HospitalCatalogue_fk_id", referencedColumnName = "hospital_id")
    private List<HospitalCatalogue> hospitalCatalogue;
    
	
	
	public Hospital() {
	}


	public Hospital(int hospitalid, String hospitaladdress, String hospitalname, String hospitalimagepath,
			List<HospitalCatalogue> hospital_catalogue) {
		super();
		this.hospitalId = hospitalid;
		this.hospitalAddress = hospitaladdress;
		this.hospitalName = hospitalname;
		this.hospitalImagePath = hospitalimagepath;
		this.hospitalCatalogue = hospital_catalogue;
	}


	public Integer getHospitalId() {
		return hospitalId;
	}


	public void setHospitalId(Integer hospitalId) {
		this.hospitalId = hospitalId;
	}


	public String getHospitalAddress() {
		return hospitalAddress;
	}


	public void setHospitalAddress(String hospitalAddress) {
		this.hospitalAddress = hospitalAddress;
	}


	public String getHospitalName() {
		return hospitalName;
	}


	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}


	public String getHospitalImagePath() {
		return hospitalImagePath;
	}


	public void setHospitalImagePath(String hospitalImagePath) {
		this.hospitalImagePath = hospitalImagePath;
	}


	public List<HospitalCatalogue> getHospitalCatalogue() {
		return hospitalCatalogue;
	}


	public void setHospitalCatalogue(List<HospitalCatalogue> hospitalCatalogue) {
		this.hospitalCatalogue = hospitalCatalogue;
	}


}