package com.hollongtree.ehospital.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "patient_appointment_history")
public class PatientAppointmentHistory {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "patient_appointment_history_id")
	private int patientAppointmentHistoryId;
	@Column(name = "appointment_start_time")
	private Date appointmentStartTime;
	@Column(name = "appointment_end_time")
	private Date appointmentEndTime;
	@Column(name = "expireStatus")
	private String expireStatus;
	@Column(name = "patients_appointment_details_id")
	@ElementCollection(targetClass = PatientPaymentDetails.class)
	private Set<PatientPaymentDetails> patientsAppointmentDetailsId;
	@Column(name = "appointment_is_done")
	private boolean appointmentIsDone;
	@Column(name = "doctor_details_id")
	private int doctorDetailsId;
	@Column(name = "videoLink")
	private String videoLink;
	@Column(name = "appointment_status")
	private String appointmentStatus;
	
	public PatientAppointmentHistory() {
	}

	public PatientAppointmentHistory(int patientAppointmentHistoryId, Date appointmentStartTime,
			Date appointmentEndTime, String expireStatus, Set<PatientPaymentDetails> patientsAppointmentDetailsId, boolean appointmentIsDone,
			int doctorDetailsId, String videoLink, String appointmentStatus) {
		super();
		this.patientAppointmentHistoryId = patientAppointmentHistoryId;
		this.appointmentStartTime = appointmentStartTime;
		this.appointmentEndTime = appointmentEndTime;
		this.expireStatus = expireStatus;
		this.patientsAppointmentDetailsId = patientsAppointmentDetailsId;
		this.appointmentIsDone = appointmentIsDone;
		this.doctorDetailsId = doctorDetailsId;
		this.videoLink = videoLink;
		this.appointmentStatus = appointmentStatus;
	}

	public int getPatientAppointmentHistoryId() {
		return patientAppointmentHistoryId;
	}

	public void setPatientAppointmentHistoryId(int patientAppointmentHistoryId) {
		this.patientAppointmentHistoryId = patientAppointmentHistoryId;
	}

	public Date getAppointmentStartTime() {
		return appointmentStartTime;
	}

	public void setAppointmentStartTime(Date appointmentStartTime) {
		this.appointmentStartTime = appointmentStartTime;
	}

	public Date getAppointmentEndTime() {
		return appointmentEndTime;
	}

	public void setAppointmentEndTime(Date appointmentEndTime) {
		this.appointmentEndTime = appointmentEndTime;
	}

	public String getExpireStatus() {
		return expireStatus;
	}

	public void setExpireStatus(String expireStatus) {
		this.expireStatus = expireStatus;
	}

	@ManyToMany(fetch = FetchType.LAZY,mappedBy = "patient_appointment_history")
	public Set<PatientPaymentDetails> getPatientsAppointmentDetailsId() {
		return patientsAppointmentDetailsId;
	}

	public void setPatientsAppointmentDetailsId(Set<PatientPaymentDetails> patientsAppointmentDetailsId) {
		this.patientsAppointmentDetailsId = patientsAppointmentDetailsId;
	}

	public boolean isAppointmentIsDone() {
		return appointmentIsDone;
	}

	public void setAppointmentIsDone(boolean appointmentIsDone) {
		this.appointmentIsDone = appointmentIsDone;
	}

	public int getDoctorDetailsId() {
		return doctorDetailsId;
	}

	public void setDoctorDetailsId(int doctorDetailsId) {
		this.doctorDetailsId = doctorDetailsId;
	}

	public String getVideoLink() {
		return videoLink;
	}

	public void setVideoLink(String videoLink) {
		this.videoLink = videoLink;
	}

	public String getAppointmentStatus() {
		return appointmentStatus;
	}

	public void setAppointmentStatus(String appointmentStatus) {
		this.appointmentStatus = appointmentStatus;
	}
	
	

}
