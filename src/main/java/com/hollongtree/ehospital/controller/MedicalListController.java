package com.hollongtree.ehospital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hollongtree.ehospital.entity.MedicalList;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.MedicalListService;

@RestController
public class MedicalListController {

	@Autowired
	MedicalListService medicalListService;

	/**
	 * API for adding the value of MedicalList to the db for that passing the values
	 * to the service class
	 * 
	 * @param medicalList
	 * @return
	 */
	@PostMapping(path = "/medicalList")
	public ResponseEntity<MedicalList> addEntryMedicalList(@RequestBody MedicalList medicalList) {
		MedicalList ml = medicalListService.addMedicalList(medicalList);
		return ResponseEntity.status(HttpStatus.OK).body(ml);
	}

	/**
	 * API for fetching the data using medicalName and handling the exception if
	 * there is any null value
	 * 
	 * @param medicalName
	 * @return
	 * @throws EhospitalException
	 */
	@GetMapping(path = "/medicalList/{medicalName}")
	public ResponseEntity<MedicalList> getMedicalList(@PathVariable("medicalName") String medicalName)
			throws EhospitalException {
		MedicalList ml = medicalListService.getMedicalList(medicalName);

		return ResponseEntity.status(HttpStatus.OK).body(ml);
	}

	/**
	 * Updating the medicallist passing the value to the service class
	 * @param medicalList
	 * @return
	 */
	@PutMapping(path = "/medicalListUpdate")
	public ResponseEntity<MedicalList> updateMedicalList(@RequestBody MedicalList medicalList) {
		MedicalList ml = medicalListService.updateMedicalList(medicalList);
		return ResponseEntity.status(HttpStatus.OK).body(ml);
	}

}
