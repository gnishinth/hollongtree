package com.hollongtree.ehospital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hollongtree.ehospital.entity.DoctorDetails;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.DoctorDetailsService;

@RestController
public class DoctorDetailsController {

	@Autowired
	DoctorDetailsService doctorDetailsService;

	/**
	 * API for passing the data to the service class
	 * 
	 * @param doctordetails
	 * @return
	 */
	@PostMapping(path = "/doctorDetails")
	public ResponseEntity<DoctorDetails> addEntryDoctorDetails(DoctorDetails doctordetails) {
		DoctorDetails dd = doctorDetailsService.addDoctorDetails(doctordetails);
		return ResponseEntity.status(HttpStatus.OK).body(dd);

	}

	/**
	 * API for fetching the data from doctor_details entity, passing the data to the
	 * service class
	 * 
	 * @param doctorname
	 * @return
	 * @throws EhospitalException
	 */
	@GetMapping(path = "/doctorDetails/{doctorname}")
	public ResponseEntity<DoctorDetails> getDoctorDetails(@PathVariable("doctorname") String doctorname)
			throws EhospitalException {
		DoctorDetails dd = doctorDetailsService.getDoctorDetails(doctorname);
		return ResponseEntity.status(HttpStatus.OK).body(dd);

	}

	/**
	 * API for updating the record in the doctor_details entity
	 * 
	 * @param doctordetails
	 * @return
	 */
	@PutMapping(path = "/doctorDetailsUpdate")
	public ResponseEntity<DoctorDetails> saveOrUpdateDoctorDetails(@RequestBody DoctorDetails doctordetails) {
		DoctorDetails dd = doctorDetailsService.updateDoctorDetails(doctordetails);
		return ResponseEntity.status(HttpStatus.OK).body(dd);

	}

}
