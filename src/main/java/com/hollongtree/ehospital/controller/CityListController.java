package com.hollongtree.ehospital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hollongtree.ehospital.entity.CityList;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.CityListService;

@RestController
public class CityListController {

	@Autowired
	CityListService cityListService;

	/**
	 * API for sending the data to the service class
	 * 
	 * @param cityList
	 * @return Httpstatus
	 */
	@PostMapping(path = "/cityList")
	public ResponseEntity<CityList> addEntryCityList(@RequestBody CityList cityList) {
		CityList cl = cityListService.addCityList(cityList);
		return ResponseEntity.status(HttpStatus.OK).body(cl);

	}

	/**
	 * API for fetching data from City list entity, passing the data to service class
	 * 
	 * @param cityName
	 * @return
	 * @throws EhospitalException
	 */

	@GetMapping(path = "/cityList/{cityName}")
	public ResponseEntity<CityList> addEntryCityList(@PathVariable("cityName") String cityName)
			throws EhospitalException {

		CityList cl = cityListService.getCityList(cityName);

		return ResponseEntity.status(HttpStatus.OK).body(cl);

	}

	/**
	 * API for Update Citylist passing the data to service class
	 * 
	 * @param cityList
	 * @return
	 */

	@PutMapping(path = "/cityListUpdate")
	public ResponseEntity<CityList> saveOrupdateCityList(@RequestBody CityList cityList) {
		CityList cl = cityListService.updateCityList(cityList);
		return ResponseEntity.status(HttpStatus.OK).body(cl);

	}

}
