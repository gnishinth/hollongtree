package com.hollongtree.ehospital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hollongtree.ehospital.entity.HospitalCatalogue;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.HospitalCatalogueService;

@RestController
public class HospitalCatalogueController {

	@Autowired
	HospitalCatalogueService hospitalCatalogueService;

	/**
	 * Api for adding the values of hospitalcatalogue to the db for that passing the
	 * param to the service class
	 * 
	 * @param hospitalcatalogue
	 * @return
	 */
	@PostMapping(path = "/hospitalCatalogue")
	public ResponseEntity<HospitalCatalogue> addEntryHospitalCatalogue(HospitalCatalogue hospitalcatalogue) {
		HospitalCatalogue hc = hospitalCatalogueService.addHospitalCatalogue(hospitalcatalogue);
		return ResponseEntity.status(HttpStatus.OK).body(hc);
	}

	/**
	 * Api for fetching the data using catalogue name and handling exception if
	 * there is any null value
	 * 
	 * @param catalogueName
	 * @return response entity and httpstatus
	 */
	@GetMapping("/hospitalCatalogue/{catalogueName}")
	public ResponseEntity<HospitalCatalogue> getHospitalCatalogue(@PathVariable("catalogueName") String catalogueName) {
		HospitalCatalogue hc = null;
		try {
			hc = hospitalCatalogueService.getHospitalCatalogue(catalogueName);
		} catch (EhospitalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.status(HttpStatus.OK).body(hc);
	}

	/**
	 * Update hospitalcatalogue passing the value to the service class
	 * 
	 * @param hospitalCatalogue
	 * @return reponse entity and http status
	 */
	@PutMapping(path = "/hospitalCatalogueupdate")
	public ResponseEntity<HospitalCatalogue> saveOrupdateHospitalCatalogue(
			@RequestBody HospitalCatalogue hospitalCatalogue) {
		HospitalCatalogue hcat = hospitalCatalogueService.updateHospitalCatalogue(hospitalCatalogue);
		return ResponseEntity.status(HttpStatus.OK).body(hcat);
	}

}
