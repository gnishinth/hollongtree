package com.hollongtree.ehospital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hollongtree.ehospital.entity.MedicineList;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.MedicineListService;

@RestController
public class MedicineListController {

	@Autowired
	MedicineListService medicineListService;

	/**
	 * API for adding the data to the db, passing the values to the service class
	 * 
	 * @param medicineList
	 * @return
	 */
	@PostMapping(path = "/medicineList")
	public ResponseEntity<MedicineList> addEntryMedicineList(MedicineList medicineList) {
		MedicineList ml = medicineListService.addMedicineList(medicineList);
		return ResponseEntity.status(HttpStatus.OK).body(ml);
	}

	/**
	 * API for fetching the data from the db using medicinename if not found then
	 * throw a exception
	 * 
	 * @param medicineName
	 * @return
	 * @throws EhospitalException
	 */
	@GetMapping(path = "medicineList/{medicineName}")
	public ResponseEntity<MedicineList> getMedicineList(@PathVariable("medicineName") String medicineName)
			throws EhospitalException {
		MedicineList ml = medicineListService.getMedicineList(medicineName);

		return ResponseEntity.status(HttpStatus.OK).body(ml);
	}

	/**
	 * API for updating the medicalList entity, passing the value to the service
	 * class
	 * 
	 * @param medicineList
	 * @return
	 */
	@PutMapping(path = "/medicineListUpdate")
	public ResponseEntity<MedicineList> updateMedicineList(@RequestBody MedicineList medicineList) {
		MedicineList ml = medicineListService.updateMedicineList(medicineList);
		return ResponseEntity.status(HttpStatus.OK).body(ml);
	}

}
