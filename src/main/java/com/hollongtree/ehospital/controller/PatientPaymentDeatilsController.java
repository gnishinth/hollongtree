package com.hollongtree.ehospital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hollongtree.ehospital.entity.PatientPaymentDetails;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.PatientPaymentDeatilsService;

@RestController
public class PatientPaymentDeatilsController {

	@Autowired
	PatientPaymentDeatilsService patientPaymentDeatilsService;

	/**
	 * API for save the patientPaymentDeatils, for that passing the value to the
	 * service class
	 * 
	 * @param patientPaymentDeatils
	 * @return
	 */
	@PostMapping(path = "/patientPaymentDeatils")
	public ResponseEntity<PatientPaymentDetails> addEntryPatientPaymentDeatils(
			PatientPaymentDetails patientPaymentDeatils) {
		PatientPaymentDetails ppd = patientPaymentDeatilsService.addPatientPaymentDetails(patientPaymentDeatils);
		return ResponseEntity.status(HttpStatus.OK).body(ppd);

	}

	/**
	 * API for fetch the data from the patientPaymentDeatils entity using
	 * patientsname
	 * 
	 * @param patientsName
	 * @return
	 * @throws EhospitalException
	 */
	@GetMapping("/patientPaymentDeatils/patientsName")
	public ResponseEntity<PatientPaymentDetails> getPatientPaymentDetails(
			@PathVariable("patientsName") String patientsName) throws EhospitalException {
		PatientPaymentDetails ppd = patientPaymentDeatilsService.getPatientPaymentDetails(patientsName);

		return ResponseEntity.status(HttpStatus.OK).body(ppd);
	}

	/**
	 * API for Update patientPaymentDeatils entity
	 * 
	 * @param patientPaymentDeatils
	 * @return
	 */
	@PutMapping(path = "/patientPaymentDeatilsUpdate")
	public ResponseEntity<PatientPaymentDetails> saveOrUspdatePatientPaymentDeatils(
			@RequestBody PatientPaymentDetails patientPaymentDeatils) {
		PatientPaymentDetails ppd = patientPaymentDeatilsService.updatePatientPaymentDetails(patientPaymentDeatils);
		return ResponseEntity.status(HttpStatus.OK).body(ppd);

	}

}
