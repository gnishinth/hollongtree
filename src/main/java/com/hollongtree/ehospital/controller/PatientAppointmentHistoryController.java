package com.hollongtree.ehospital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hollongtree.ehospital.entity.PatientAppointmentHistory;
import com.hollongtree.ehospital.service.PatientAppointmentHistoryService;

@RestController
public class PatientAppointmentHistoryController {
	
	@Autowired
	PatientAppointmentHistoryService patientAppointmentHistoryService;
	
	/**
	 * API for save the data to the patientAppointmentHistory, passing the value to the service class
	 * @param patientAppointmentHistory
	 * @return
	 */
	@PostMapping(path = "/patientAppointmentHistory")
	public ResponseEntity<PatientAppointmentHistory> addEntryPatientAppointmentHistory(PatientAppointmentHistory patientAppointmentHistory)
	{
		PatientAppointmentHistory pah=patientAppointmentHistoryService.addPatientAppointmentHistory(patientAppointmentHistory);
		return ResponseEntity.status(HttpStatus.OK).body(pah);
		
	}

	/**
	 * API for Updating the patientAppointmentHistory entity
	 * @param patientAppointmentHistory
	 * @return
	 */
	@PutMapping(path = "/patientAppointmentHistoryupdate")
	public ResponseEntity<PatientAppointmentHistory> saveOrUpdatePatientAppointmentHistory(PatientAppointmentHistory patientAppointmentHistory)
	{
		PatientAppointmentHistory pah=patientAppointmentHistoryService.updatePatientAppointmentHistory(patientAppointmentHistory);
		return ResponseEntity.status(HttpStatus.OK).body(pah);
		
	}

}
