package com.hollongtree.ehospital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hollongtree.ehospital.entity.HospitalCategory;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.HospitalCategoryService;

@RestController
public class HospitalCategoryController {

	@Autowired
	HospitalCategoryService hospitalCategoryService;

	/**
	 * Api for adding the values of hospitalcategory entity into the DB for that
	 * passing the value to the service class
	 * 
	 * @param hospitalCategory
	 * @return responseentity httpstatus of the code
	 */
	@PostMapping(path = "/hospitalCategory")
	public ResponseEntity<HospitalCategory> addEntryHospitalCategory(HospitalCategory hospitalCategory) {
		HospitalCategory hc = hospitalCategoryService.addHospitalCategory(hospitalCategory);
		return ResponseEntity.status(HttpStatus.OK).body(hc);
	}

	/**
	 * Api for fetching the data using hospitalcategoryname and handling the
	 * exception if there is any null value
	 * 
	 * @param hospitalCategoryName
	 * @return
	 */
	@GetMapping(path = "/hospitalCategory/{hospitalCategoryName}")
	public ResponseEntity<HospitalCategory> getCategory(
			@PathVariable("hospitalCategoryName") String hospitalCategoryName) {
		HospitalCategory id = null;
		try {
			id = hospitalCategoryService.getCategory(hospitalCategoryName);
		} catch (EhospitalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ResponseEntity.status(HttpStatus.OK).body(id);
	}

	/**
	 * Api for updating data passing the param to the service class
	 * 
	 * @param hospitalCategory
	 * @return httpstatus of the code
	 */
	@PutMapping(path = "/hospitalCategoryUpdate")
	public ResponseEntity<HospitalCategory> saveOrUpdateHospitalCategory(
			@RequestBody HospitalCategory hospitalCategory) {
		HospitalCategory hc = hospitalCategoryService.updateHospitalCategory(hospitalCategory);
		return ResponseEntity.status(HttpStatus.OK).body(hc);
	}

}
