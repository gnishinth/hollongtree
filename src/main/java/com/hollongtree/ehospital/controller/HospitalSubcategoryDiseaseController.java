package com.hollongtree.ehospital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hollongtree.ehospital.entity.HospitalSubcategoryDisease;
import com.hollongtree.ehospital.service.HospitalSubcategoryDiseaseService;

@RestController
public class HospitalSubcategoryDiseaseController {

	@Autowired
	HospitalSubcategoryDiseaseService hospitalSubcategoryDiseaseService;

	/**Api for adding the values of hospitalSubcategoryDisease to the db
	 * for that passing the param to the service class
	 * @param hospitalcatalogues
	 * @return responseentity and httpstatus of the code
	 */
	@PostMapping(path = "/hospitalSubcategoryDisease")
	public ResponseEntity<HospitalSubcategoryDisease> addEntryHospitalSubcategoryDisease(
			 HospitalSubcategoryDisease hospitalSubcategoryDisease) 
	{
		HospitalSubcategoryDisease hsd = hospitalSubcategoryDiseaseService
					.addHospitalSubcategoryDisease(hospitalSubcategoryDisease);
		
		return ResponseEntity.status(HttpStatus.OK).body(hsd);
	}
	/** Api for fetching the data using hospitalSubcategoryName
	 * and handling the exception if there is any null value
	 * @param hospitalCategoryName
	 * @return response entity and httpstatus of the code
	 */

	@GetMapping(path = "/hospitalSubcategoryDisease/{hospitalSubcategoryName}")
	public ResponseEntity<HospitalSubcategoryDisease> getHospitalSubcategoryDisease(
			@PathVariable("hospitalSubcategoryName") String hospitalSubcategoryName) {
		HospitalSubcategoryDisease hsd = null;
		try {
			hsd = hospitalSubcategoryDiseaseService
					.getHospitalSubcategoryDisease(hospitalSubcategoryName);
		} catch (Exception e) {
		}
		return ResponseEntity.status(HttpStatus.OK).body(hsd);
	}


	/** Api for updating data 
	 * passing the param to the service class
	 * @param hospitalCategory
	 * @return httpstatus of the code
	 */
	@PutMapping(path = "/hospitalSubcategoryDiseaseupdate")
	public ResponseEntity<HospitalSubcategoryDisease> saveOrUpdateHospitalSubcategoryDisease(
			@RequestBody HospitalSubcategoryDisease hospitalSubcategoryDisease) {
		HospitalSubcategoryDisease hsd = hospitalSubcategoryDiseaseService
				.updateHospitalSubcategoryDisease(hospitalSubcategoryDisease);
		return ResponseEntity.status(HttpStatus.OK).body(hsd);
	}

}
