package com.hollongtree.ehospital.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hollongtree.ehospital.entity.CountryList;
import com.hollongtree.ehospital.exception.EhospitalException;
import com.hollongtree.ehospital.service.CountryListService;

@RestController
public class CountryListController {
	
	@Autowired
	CountryListService countryListService;
	
	/**API for adding data
	 * passing the data to the service class
	 * @param countryList
	 * @return
	 */
	@PostMapping(path= "/countryList")
	public ResponseEntity<CountryList> addEntryCountryList(@RequestBody CountryList countryList)
	{
		CountryList cl=countryListService.addCountryList(countryList);
		return ResponseEntity.status(HttpStatus.OK).body(cl);
		
	}
	/**
	 * API for fetching the data from the database using countryname
	 * @param countryName
	 * @return
	 * @throws EhospitalException
	 */
	@GetMapping(path= "/countryList/{countryName}")
	public ResponseEntity<CountryList> addEntryCountryList(@PathVariable("countryName")String countryName) throws EhospitalException
	{
		CountryList cl = countryListService.addCountryList(countryName);
		
		return ResponseEntity.status(HttpStatus.OK).body(cl);
		
	}
	/**
	 * API for updating the records in the Database
	 * @param countryList
	 * @return
	 */
	@PutMapping(path= "/countryListUpdate")
	public ResponseEntity<CountryList> updateCountryList(@RequestBody CountryList countryList)
	{
		CountryList cl=countryListService.updateCountryList(countryList);
		return ResponseEntity.status(HttpStatus.OK).body(cl);
		
	}
	
	

}
